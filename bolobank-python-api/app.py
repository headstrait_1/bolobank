import os
import dialogflow_v2 as dialogflow
os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="FAQ-iguxxk-038df156ed3a.json" 
from flask import request, jsonify, Flask
from flask_cors import CORS
from google.cloud import translate_v3beta1 as translate
from google.protobuf.json_format import MessageToDict
from google.cloud import texttospeech
import simplejson as json
import base64


PROJECT_ID = 'faq-iguxxk'
app = Flask(__name__)
CORS(app)

@app.route('/dialogflow', methods=["POST", "GET"])
def Dialogflow():
#     """ 

#     Returns the balance in a User's account \n
#     ::: Accepts 2 parameters: \n
#     language:  System Language selected by the User \n
#     account_type: The type of account whose balance is to be retrieved (Ex: Savings or Current)
    
#     """
	sessionID = request.get_json().get('sessionId', '')
	query = request.get_json().get('query', '')

	session_client = dialogflow.SessionsClient()
	session = session_client.session_path('faq-iguxxk', 'unique')
	text_input = dialogflow.types.TextInput(
	text=query, language_code='en-IN')

	query_input = dialogflow.types.QueryInput(text=text_input)

	output_audio_config = dialogflow.types.OutputAudioConfig(
	audio_encoding=dialogflow.enums.OutputAudioEncoding
	.OUTPUT_AUDIO_ENCODING_MP3)

	response = session_client.detect_intent(
	session=session, query_input=query_input,
	output_audio_config=output_audio_config)

	return jsonify({'result': response.query_result.fulfillment_text})


@app.route('/translate-to-eng', methods=["POST", "GET"])
def TranslateToEng():
#     """
 
#	Accepts Text and Language to translate the given text to English

#     """
	text = request.get_json().get('text', '')
	language = request.get_json().get('language', '')
	location = 'global'

	client = translate.TranslationServiceClient()
	parent = client.location_path(PROJECT_ID, location)

	response = client.translate_text(
	    parent=parent,
	    contents=[text],
	    mime_type='text/plain',  # mime types: text/plain, text/html
	    source_language_code=language,
	    target_language_code='en-IN')

	response_dict = MessageToDict(response, preserving_proto_field_name = True)
	translated_text = response_dict['translations'][0]['translated_text']
	return jsonify({'result' : translated_text})

@app.route('/translate-from-eng', methods=["POST", "GET"])
def TranslateFromEng():
#     """
 
#	Accepts Text and Language to translate the given text to English

#     """
	text = request.get_json().get('text', '')
	language = request.get_json().get('language', '')
	location = 'global'

	client = translate.TranslationServiceClient()
	parent = client.location_path(PROJECT_ID, location)

	response = client.translate_text(
	    parent=parent,
	    contents=[text],
	    mime_type='text/plain',  # mime types: text/plain, text/html
	    source_language_code='en-IN',
	    target_language_code=language)

	response_dict = MessageToDict(response, preserving_proto_field_name = True)
	translated_text = response_dict['translations'][0]['translated_text']
	return jsonify({'result' : translated_text})

@app.route('/text-to-speech', methods=["POST", "GET"])
def TextToSpeech():
#     """
 
#	Accepts Text and Language to return binary audio file

#     """

	text = request.get_json().get('text', '')
	# language = request.get_json().get('language', '')

	# Instantiates a client
	client = texttospeech.TextToSpeechClient()

	# Set the text input to be synthesized
	synthesis_input = texttospeech.types.SynthesisInput(text=text)

	# Build the voice request, select the language code ("en-US") and the ssml
	# voice gender ("neutral")
	voice = texttospeech.types.VoiceSelectionParams(name='hi-IN-Wavenet-A',
		language_code='hi-IN',
		ssml_gender=texttospeech.enums.SsmlVoiceGender.NEUTRAL)

	# Select the type of audio file you want returned
	audio_config = texttospeech.types.AudioConfig(	
		audio_encoding=texttospeech.enums.AudioEncoding.MP3,)

	# Perform the text-to-speech request on the text input with the selected
	# voice parameters and audio file type
	response = client.synthesize_speech(synthesis_input, voice, audio_config)
	# print(type(response.audio_content))
	data = response.audio_content

	# print(data.decode('ISO-8859-1'))
	return jsonify({'result' :  base64.b64encode(response.audio_content).decode('ascii')})
	# return data
if __name__ == '__main__':
    app.run(debug=True)
    # app.run()
