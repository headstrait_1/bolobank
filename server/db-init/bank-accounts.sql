DROP database if exists bolobank;
create database bolobank;

\c bolobank;

create table users(
    id SERIAL PRIMARY KEY,
    fname VARCHAR(30),
    lname VARCHAR(30),
    email  VARCHAR(100),
    phone_number VARCHAR(20),
    dob date,
    pin VARCHAR(120)
);

create table otp(
    id SERIAL PRIMARY KEY,
    phone_number VARCHAR(30),
    otp varchar(30),
    time_created timestamp default current_timestamp,
    time_valid timestamp default current_timestamp + INTERVAL '1 hour',
    used VARCHAR(30) default 0
);

create table account_types(
    id SERIAL PRIMARY KEY,
    account_type VARCHAR (30)
);

insert into account_types(account_type) values ('wallet');
insert into account_types(account_type) values ('bank');
insert into account_types(account_type) values ('savings');
insert into account_types(account_type) values ('current');

create table benef(
    id SERIAL PRIMARY KEY,
    user_id INTEGER references users(id),
    account_type INTEGER references account_types(id),
    account_number VARCHAR(50),
    account_holder VARCHAR(150),
    ifsc_code VARCHAR(50)
);

create table bank_accounts(
    id SERIAL PRIMARY KEY,
    user_id INTEGER references users(id),
    upi_id VARCHAR(50),
    bank_account_number VARCHAR(50),
    bank_name VARCHAR(150),
    account_type INTEGER references account_types(id),
    upi_pin VARCHAR(150)
);

create table account_balance(
    id SERIAL PRIMARY KEY,
    account_number VARCHAR(50) UNIQUE,
    balance REAL
);

create table transactions(
    reference_id VARCHAR PRIMARY KEY,
    sender_id INTEGER references users(id),
    receiver_id INTEGER references benef(id),
    sender_account_number VARCHAR(50),
    receiver_account_number VARCHAR(50),
    receiver_account_type VARCHAR(50),
    amount REAL,
    status boolean,
    timestamp VARCHAR(150),
    message VARCHAR(150)
);

create table balance_change(
    transaction_id VARCHAR references transactions(reference_id),
    account_number VARCHAR(50),
    transaction_type VARCHAR(20),
    amount REAL,
    timestamp VARCHAR(150),
    balance_before REAL,
    balance_after REAL
);