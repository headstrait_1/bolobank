const pg = require("pg-promise")();
const config = require("../config");
const postgresURL = config.postresURL;
const db = pg(postgresURL);

module.exports = db;
