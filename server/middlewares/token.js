const jwt = require("jsonwebtoken");
const jwtPrivateKey = require("../config").jwtPrivateKey;

module.exports.generateToken = (payload, expiresIn) => {
  return jwt.sign(payload, jwtPrivateKey, { expiresIn });
};
