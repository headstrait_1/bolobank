const joi = require("@hapi/joi");

module.exports.validateUser = user => {
  const userSchema = {
    fname: joi,
    lname: joi,
    phone_number: joi,
    email: joi,
    dob: joi,
    user_pin: joi
  };
  return joi.validate(user, userSchema);
};
