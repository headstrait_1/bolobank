const express = require("express");
const app = express();
const bodyParser = require("body-parser");
// const products = require("./routes/api/products");
const users = require("./routes/api/users");
const beneficiaries = require("./routes/api/beneficiaries");
const transactions = require("./routes/api/transactions");
const bankAccounts = require("./routes/api/bank-accounts");
const cors = require("cors");
const logger = require("morgan");
const auth = require("./middlewares/auth");
const error = require("./middlewares/error");
const dateFormat = require('dateformat');
console.log(dateFormat(new Date(), "mmm dd yyyy HH:MM:ss" ));

app.use(bodyParser.json());
app.use(cors());
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);

app.use(logger("common"));
app.use("/api/users/", users);
app.use("/api/beneficiaries/", auth, beneficiaries);
app.use("/api/transactions", auth, transactions);
app.use("/api/bank-accounts", auth, bankAccounts);
// console.log(new Date())
app.use(error);

const port = process.env.PORT || 5000;

if (process.env.NODE_ENV !== "test")
  app.listen(port, () =>
    console.log(`Server is listening at http://localhost:${port}`)
  );

module.exports = app;
