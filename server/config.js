const postresURL = "postgres://postgres:root@130.211.248.177:5432/bolobank";
const jwtPrivateKey = "shady";
const TABLE_USERS = "users";
const TABLE_OTP = "otp";
const TABLE_BENEF = "benef";
const TABLE_TRANSACTIONS = "transactions";
const TABLE_BANK_ACCOUNTS = "bank_accounts";
const TABLE_BALANCE_CHANGE = "balance_change";
const TABLE_ACCOUNT_BALANCE = "account_balance";

// Twilio Config
const twilioAccountSid = "AC6863b31392337dabd6f82b3abb29395a";
const twilioAuthToken = "4a7d0361f6778a1290f87370c4636f03";
const twilioNumber = "+12512570824";

module.exports = {
  postresURL,
  jwtPrivateKey,
  TABLE_USERS,
  TABLE_OTP,
  twilioAccountSid,
  twilioAuthToken,
  twilioNumber,
  TABLE_BENEF,
  TABLE_TRANSACTIONS,
  TABLE_BANK_ACCOUNTS,
  TABLE_BALANCE_CHANGE,
  TABLE_ACCOUNT_BALANCE
};
