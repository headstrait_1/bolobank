const uuidv1 = require("uuid/v1");
const dateFormat = require("dateformat");

const performTransaction = () => {
  const referenceNumber = uuidv1();
  return {
    status: true,
    referenceNumber,
    timestamp: Date.now(),
    message: "Transaction Successful"
  };
};

module.exports = performTransaction;
