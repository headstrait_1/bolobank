const express = require("express");
const router = express.Router();
const config = require("../../config");
const TABLE_TRANSACTIONS = config.TABLE_TRANSACTIONS;
const TABLE_BANK_ACCOUNTS = config.TABLE_BANK_ACCOUNTS;
const TABLE_BALANCE_CHANGE = config.TABLE_BALANCE_CHANGE;
const TABLE_ACCOUNT_BALANCE = config.TABLE_ACCOUNT_BALANCE;
const db = require("../../db-init/db-conn");
const performTransaction = require("../../utils/performTransaction");
const TRANSACTION_TYPE_DEBIT = "DB";
const TRANSACTION_TYPE_CREDIT = "CR";

// Route to transfer money from Sender's account to Receiver's account 
// Body Parameters: {
// senderBankInfo{selectedBankAccNumber: value, selectedBankName: value, selectedBankUPIid: value}
// receiverData{account_number:value, account_holder_name: value, account_type: value}
// amount 
// }
router.post("/send", async (req, res, next) => {
  const { senderBankInfo, receiverData, amount } = req.body;
  const { user_id } = req.user;

  if (typeof senderBankInfo !== "undefined") {
    const {
      status,
      timestamp,
      referenceNumber,
      message
    } = performTransaction();

    try {
      sender_account_balance = await db.any(
        `select balance from ${TABLE_ACCOUNT_BALANCE} where account_number='${senderBankInfo.selectedBankAccNumber}'`
      );

      if (amount > sender_account_balance[0].balance) {
        res.status(400).json({
          status: 400,
          message: "Entered amount is greater than the available balance"
        });
      } else {
        // Getting balance of Receiver
        receiver_account_balance = await db.any(
          `select balance from ${TABLE_ACCOUNT_BALANCE} where account_number='${receiverData.account_number}'`
        );

        sender_new_account_balance =
          sender_account_balance[0].balance - parseInt(amount);
        receiver_new_account_balance =
          receiver_account_balance[0].balance + parseInt(amount);

        // Updating Balance of Sender
        await db.none(
          `update ${TABLE_ACCOUNT_BALANCE} set balance = ${sender_new_account_balance} where account_number='${senderBankInfo.selectedBankAccNumber}'`
        );

        // Updating Balance of Receiver
        await db.none(
          `update ${TABLE_ACCOUNT_BALANCE} set balance = ${receiver_new_account_balance} where account_number='${receiverData.account_number}'`
        );

        // Adding transaction details in Transaction Table
        await db.none(
          `insert into ${TABLE_TRANSACTIONS} (reference_id, sender_id, receiver_id, sender_account_number, receiver_account_number, receiver_account_type, amount, status, timestamp, message) values('${referenceNumber}', '${user_id}', '${receiverData.id}', '${senderBankInfo.selectedBankAccNumber}', '${receiverData.account_number}', '${receiverData.account_type}', '${amount}', '${status}', '${timestamp}', '${message}')`
        );

        // Adding Balance updation details in Balance Change Table
        // For Sender
        await db.none(
          `insert into ${TABLE_BALANCE_CHANGE} (transaction_id, account_number, transaction_type, amount, timestamp, balance_before, balance_after) values('${referenceNumber}','${senderBankInfo.selectedBankAccNumber}', '${TRANSACTION_TYPE_DEBIT}' , '${amount}', '${timestamp}', '${sender_account_balance[0].balance}', '${sender_new_account_balance}')`
        );

        // For Receiver
        await db.none(
          `insert into ${TABLE_BALANCE_CHANGE} (transaction_id, account_number, transaction_type, amount, timestamp, balance_before, balance_after) values('${referenceNumber}','${receiverData.account_number}', '${TRANSACTION_TYPE_CREDIT}' , '${amount}', '${timestamp}', '${receiver_account_balance[0].balance}', '${receiver_new_account_balance}')`
        );

        res.status(200).json({
          message,
          referenceNumber,
          status: 200
        });
      }
    } catch (error) {
      console.log(error);
    }
  }
});

// Route to get all the transactions of a single user
router.get("/all", async (req, res, next) => {
  try {
    const { user_id } = req.user;

    // Query for where user is the sender
    let QUERY = `SELECT t.*, b.account_holder as receiver_name, CONCAT(u.fname, ' ', u.lname) as sender_name
    FROM transactions t
    LEFT JOIN benef b ON t.receiver_account_number=b.account_number
    LEFT JOIN users u ON t.sender_id=u.id
    WHERE t.sender_id = ${user_id} `;

    const bankAcconts = await db.any(
      `select bank_account_number from ${TABLE_BANK_ACCOUNTS} where user_id = '${user_id}'`
    );

    // Query for where user is the reciever
    bankAcconts.map(
      account =>
        (QUERY =
          QUERY +
          ` UNION SELECT t.*, b.account_holder as receiver_name, CONCAT(u.fname, ' ', u.lname) as sender_name
      FROM transactions t
      LEFT JOIN benef b ON t.receiver_account_number=b.account_number
      LEFT JOIN users u ON t.sender_id=u.id
      WHERE t.receiver_account_number = '${account.bank_account_number}' `)
    );

    QUERY = QUERY + " ORDER BY timestamp DESC ";

    const result = await db.any(QUERY);

    if (result.length == 0) {
      res.status(200).json({
        status: 200,
        message: "Request Complete",
        data: { result: "No transactions found" }
      });
    } else {
      res.status(200).json({
        status: 200,
        message: "All Transactions Retrieved",
        data: result
      });
    }
  } catch (error) {
    console.log(error);
  }
});

// Route to get all the transactions from a bank account between a date range
router.post("/range", async (req, res, next) => {
  try {
    const { dateTo, dateFrom, selectedBankAccNumber } = req.body;

    // Converting Date to UNIX Timestamp since that is how it is stored in the DB
    var dateToUnixTimestamp = Math.round(new Date(dateTo).getTime() / 1000);
    var dateFromUnixTimestamp = Math.round(new Date(dateFrom).getTime() / 1000);

    // Query for where selectedBankAccNumber is the sender_account_number and receiver_account_number
    let QUERY = `SELECT b.*, t.receiver_account_number
    FROM balance_change b
    LEFT JOIN transactions t ON t.reference_id=b.transaction_id
    WHERE b.account_number = '${selectedBankAccNumber}' and (${dateFromUnixTimestamp} <= cast(b.timestamp as bigint) or cast(b.timestamp as bigint) <= ${dateToUnixTimestamp}) ORDER BY timestamp DESC;`;

    const result = await db.any(QUERY);

    if (result.length == 0) {
      res.status(200).json({
        status: 200,
        message: "Request Complete",
        data: { result: "No transactions found" }
      });
    } else {
      res.status(200).json({
        status: 200,
        message: "All Transactions Retrieved",
        data: {result}
      });
    }
  } catch (error) {
    console.log(error);
  }
});

// UNUSED ROUTE
router.get("/one/:refId", async (req, res, next) => {
  const { refId } = req.params;
  const result = await db.one(
    `select * from ${TABLE_TRANSACTIONS} where reference_id = '${refId}'`
  );
  res.status(200).json({
    status: 200,
    message: "Transaction Retrieved",
    data: result
  });
});

module.exports = router;
