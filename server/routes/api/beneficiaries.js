const express = require("express");
const router = express.Router();
const config = require("../../config");
const TABLE_BENEF = config.TABLE_BENEF;
const TABLE_ACCOUNT_BALANCE = config.TABLE_ACCOUNT_BALANCE;
const db = require("../../db-init/db-conn");

// Route to add a beneficiary
// Body Parameters: {type, acc_num, acc_name, ifsc_code (only if beneficiary is a bank account, now necessary for wallet) }
router.post("/addBenef", async (req, res, next) => {
  try {
    let account_type = req.body.type;
    let user_id = req.user.user_id;
    let balance = 10000.0;
    if (account_type === "Bank Account") {
      let account_number = req.body.acc_num;
      let account_holder = req.body.acc_name;
      let ifsc_code = req.body.ifsc_code;

      const db_result = await db.any(
        `insert into ${TABLE_BENEF} (user_id, account_type, account_number, account_holder, ifsc_code) values ('${user_id}',2, '${account_number}', '${account_holder}', '${ifsc_code}')`
      );

      accounts = await db.any(
        `select account_number from ${TABLE_ACCOUNT_BALANCE} where account_number = '${account_number}'`
      );

      if (accounts.length === 0) {
        await db.none(
          `insert into ${TABLE_ACCOUNT_BALANCE} (account_number, balance) values('${account_number}', ${balance})`
        );
      }

      res.status(200).json({
        status: 200,
        message: "Beneficiary Added Successfully"
      });
    } else {
      let wallet_acc_num = req.body.acc_num;
      let wallet_acc_name = req.body.acc_name;

      const db_result = await db.any(
        `insert into ${TABLE_BENEF} (user_id, account_type, account_number, account_holder) values ('${user_id}',1, '${wallet_acc_num}', '${wallet_acc_name}')`
      );

      accounts = await db.any(
        `select account_number from ${TABLE_ACCOUNT_BALANCE} where account_number = '${wallet_acc_num}'`
      );

      if (accounts.length === 0) {
        await db.none(
          `insert into ${TABLE_ACCOUNT_BALANCE} (account_number, balance) values('${wallet_acc_num}', ${balance})`
        );
      }

      res.status(200).json({
        status: 200,
        message: "Wallet Added Successfully"
      });
    }
  } catch (err) {
    next(err);
  }
});

// Route to get all the beneficiaries (bank accounts) of a user
router.get("/getBenefList", async (req, res, next) => {
  try {
    let user_id = req.user.user_id;
    let db_result = await db.any(
      `SELECT * FROM ${TABLE_BENEF} where account_type = 2 and user_id = '${user_id}';`
    );

    if (db_result.length === 0) {
      return res.status(404).send("No Beneficiaries found.");
    }

    res.status(200).json({
      status: 200,
      message: "Beneficiaries retrieved",
      data: { db_result }
    });
  } catch (error) {
    next(error);
  }
});

// Route to get all the beneficiaries (wallets) of a user
router.get("/getWalletList", async (req, res, next) => {
  try {
    let user_id = req.user.user_id;
    let db_result = await db.any(
      `SELECT * FROM ${TABLE_BENEF} where account_type = 1 and user_id = '${user_id}';`
    );

    if (db_result.length === 0) {
      return res.status(404).send("No Beneficiaries found.");
    }

    res.status(200).json({
      status: 200,
      message: "Wallets retrieved",
      data: { db_result }
    });
  } catch (error) {
    next(error);
  }
});

module.exports = router;
