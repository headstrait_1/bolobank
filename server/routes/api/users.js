const express = require("express");
const router = express.Router();
const config = require("../../config");
const bcrypt = require("bcrypt");
const validateUser = require("../../models/validation").validateUser;
const generateToken = require("../../middlewares/token").generateToken;
const otpGenerator = require("otp-generator");
const nodemailer = require("nodemailer");

// Twilio Setup
// const twilioAccountSid = config.twilioAccountSid;
// const twilioAuthToken = config.twilioAuthToken;
// const twilioNumber = config.twilioNumber;
// const client = require("twilio")(twilioAccountSid, twilioAuthToken);

const TABLE_USERS = config.TABLE_USERS;
const TABLE_OTP = config.TABLE_OTP;

const db = require("../../db-init/db-conn");

// Login Route
// Body Parameters: phone_number, pin
router.post("/login", async (req, res, next) => {
  try {
    let phone_number = req.body.phone_number;
    let pin = req.body.pin;

    const db_result = await db.any(
      `select * from ${TABLE_USERS} where phone_number = '${phone_number}'`
    );

    if (db_result.length != 1)
      return res.status(404).json({ message: "Number not registered" });

    let compare_result = await bcrypt.compare(pin, db_result[0].pin);

    if (!compare_result)
      return res.status(404).json({ message: "Invalid Credentials" });

    let expiresIn = "1h";
    let userDetails = {
      user_id: db_result[0].id,
      phone_number: db_result[0].phone_number,
      email: db_result[0].email,
      dob: db_result[0].dob,
      fname: db_result[0].fname,
      lname: db_result[0].lname
    };

    const token = generateToken(userDetails, expiresIn);

    res.status(200).json({
      status: 200,
      message: "Login successful",
      data: { token }
    });
  } catch (err) {
    next(err);
  }
});

// Signup Route
// Body Parameters: fname, lname, phone_number, email, dob, pin
router.post("/signup", async (req, res, next) => {
  try {
    let db_result2 = await db.any(
      `select * from ${TABLE_USERS} where phone_number = '${req.body.phone_number}'`
    );
    if (db_result2.length != 1) {
      let user_details = {
        phone_number: req.body.phone_number,
        user_pin: req.body.pin,
        fname: req.body.fname,
        lname: req.body.lname,
        dob: req.body.dob,
        email: req.body.email
      };

      validateUser(user_details)
        .then(async val_res => {
          const salt = await bcrypt.genSalt(10);
          const user_pin_hashed = await bcrypt.hash(
            user_details.user_pin,
            salt
          );

          const result = await db.any(
            `insert into ${TABLE_USERS}(fname, lname, phone_number, email, dob, pin) values('${user_details.fname}', '${user_details.lname}' ,'${user_details.phone_number}' ,'${user_details.email}' ,'${user_details.dob}', '${user_pin_hashed}') returning id`
          );
          res.status(200).json({
            status: 200,
            data: result,
            message: "Created one user successfully"
          });
        })
        .catch(error => {
          next(error);
        });
    } else
      return res
        .status(404)
        .json({ message: "Number already exists. Please Sign In" });
  } catch (err) {
    next(err);
  }
});

// Route to send OTP to the user on email
// Body Parameters: phone_number, email
router.post("/sendOTP", async (req, res, next) => {
  try {
    let phone_number = req.body.phone_number;
    let email = req.body.email;

    const otp = otpGenerator.generate(6, {
      upperCase: false,
      specialChars: false,
      alphabets: false,
      digits: true
    });

    const db_result2 = await db.any(
      `insert into ${TABLE_OTP}(phone_number, otp) values(${phone_number}, ${otp})`
    );

    // Using Twilo service to send OTP to mobile number
    // client.messages
    //   .create({
    //     body: "Your BoloBank verification code is: " + otp,
    //     from: twilioNumber,
    //     to: "+91" + phone_number
    //   })
    //   .then(message =>
    //     console.log({
    //       statusCode: 200,
    //       customMessage: "OTP sent via SMS"
    //     })
    //   )
    // .catch(error => {
    //   // error.customMessage = error.details[0].message;
    //   next(error);
    // });

    // Using NodeMailer to send Email (Change user and pass)
    var transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "abc@gmail.com",
        pass: "abc"
      }
    });

    var mailOptions = {
      from: "abc@gmail.com",
      to: email,
      subject: "OTP for Login",
      text:
        "Please use the following OTP to complete your Sign-In on BoloBank: " +
        otp +
        "!"
    };

    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });

    res.status(200).json({
      status: 200,
      message: "OTP generated and sent",
      data: { phone_number }
    });
  } catch (err) {
    next(err);
  }
});

// Route to Resend the latest OTP sent (present in the DB) to the user
// Body Parameters: phone_number, otp
router.post("/resendOTP", async (req, res, next) => {
  try {
    let phone_number = req.body.phone_number;
    let email = req.body.email;

    let db_result = await db.any(
      `SELECT otp FROM ${TABLE_OTP} 
			where phone_number = '${phone_number}' ORDER BY time_created DESC LIMIT 1;`
    );

    if (db_result.length != 1)
      return res
        .status(404)
        .json({ message: "No OTP generated for this number" });

    // Using NodeMailer to send Email (Change user and pass)
    var transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "abc@gmail.com",
        pass: "abc"
      }
    });

    var mailOptions = {
      from: "abc@gmail.com",
      to: email,
      subject: "OTP for Login",
      text:
        "Please use the following OTP to complete your Sign-In on BoloBank: " +
        db_result[0].otp
    };

    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });

    res.status(200).json({
      status: 200,
      message: "OTP generated and sent",
      data: { phone_number }
    });
  } catch (error) {
    next(error);
  }
});

// Route to verify OTP sent to the user
// Body Parameters: phone_number, otp
router.post("/verifyOTP", async (req, res, next) => {
  try {
    let otp = req.body.otp;
    let db_result = await db.any(
      `SELECT otp, time_valid, used FROM ${TABLE_OTP} 
			where phone_number = '${req.body.phone_number}' ORDER BY time_created DESC LIMIT 1;`
    );

    if (db_result.length == 1) {
      if (db_result[0].used == "1" && db_result[0].otp == otp)
        return res
          .status(404)
          .json({ message: "OTP already used. Generate new OTP" });

      if (db_result[0].used && db_result[0].otp != otp)
        return res.status(404).json({ message: "Invalid OTP" });

      const date_time = new Date();
      date_time.setHours(date_time.getHours() - 5);
      date_time.setMinutes(date_time.getMinutes() - 30);

      if (db_result[0].time_valid < date_time)
        return res
          .status(404)
          .json({ message: "OTP expired. Generate new OTP and try again" });

      if (db_result[0].otp == otp) {
        await db
          .any(
            `UPDATE ${TABLE_OTP} set used = '1'
						where phone_number = '${req.body.phone_number}' and otp = '${db_result[0].otp}';`
          )
          .then(async () => {
            res.status(200).json({
              status: 200,
              message: "OTP verified"
            });
          })
          .catch(error => {
            console.log(error);
            next(error);
          });
      } else return res.status(404).json({ message: "Incorrect OTP" });
    } else
      return res
        .status(404)
        .json({ message: "Requested information does not exist" });
  } catch (error) {
    next(error);
  }
});

module.exports = router;
