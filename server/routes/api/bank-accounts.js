const express = require("express");
const router = express.Router();
const config = require("../../config");
const TABLE_BANK_ACCOUNTS = config.TABLE_BANK_ACCOUNTS;
const TABLE_ACCOUNT_BALANCE = config.TABLE_ACCOUNT_BALANCE;
const db = require("../../db-init/db-conn");
const bcrypt = require("bcrypt");
const account_types = [3, 4]; // 3 = Savings, 4 = Current

// Route to add a new Bank Account
// Body Parameters: { UPIId, bankAccNo, bankName, UPIPin }
router.post("/new", async (req, res, next) => {
  try {
    const { user_id } = req.user;
    const { UPIId, bankAccNo, bankName, UPIPin } = req.body;
    const salt = await bcrypt.genSalt(10);
    const upiPinHashed = await bcrypt.hash(UPIPin, salt);

    // Selecting account type at random
    const account_type =
      account_types[Math.floor(Math.random() * account_types.length)];

    // Base amount in wallet/bank account
    const balance = 10000.0;

    const result_upi_id = await db.any(
      `select upi_id from ${TABLE_BANK_ACCOUNTS} where upi_id='${UPIId}'`
    );

    if (result_upi_id.length !== 0)
      res.status(400).json({
        status: 400,
        message: "UPI ID already taken"
      });
    else {
      const result = await db.any(
        `insert into ${TABLE_BANK_ACCOUNTS} (user_id, upi_id, bank_account_number, bank_name, upi_pin, account_type) values('${user_id}', '${UPIId}', '${bankAccNo}', '${bankName}', '${upiPinHashed}','${account_type}') returning user_id, upi_id, bank_account_number, bank_name, account_type`
      );

      accounts = await db.any(
        `select account_number from ${TABLE_ACCOUNT_BALANCE} where account_number = '${bankAccNo}'`
      );
      if (accounts.length === 0) {
        await db.none(
          `insert into ${TABLE_ACCOUNT_BALANCE} (account_number, balance) values('${bankAccNo}', ${balance})`
        );
      }

      res.status(200).json({
        status: 200,
        message: "Bank Account added successfully",
        data: result
      });
    }
  } catch (error) {
    next(error);
  }
});

// Route to get all the bank accounts of a user
router.get("/all", async (req, res, next) => {
  try {
    const { user_id } = req.user;
    let result = await db.any(
      `select user_id, upi_id, bank_account_number, bank_name, account_type from ${TABLE_BANK_ACCOUNTS} where user_id = '${user_id}'`
    );

    if (result.length == 0) {
      res.status(200).json({
        message: "Request Complete",
        data: { result: "No accounts found" }
      });
    } else {
      res.status(200).json({
        status: 200,
        message: "Bank Accounts Fetched Successfully",
        data: { result }
      });
    }
  } catch (error) {
    next(error);
  }
});

// Route to get the balance of an account
// Body Parameters: { bank_account_number }
router.post("/balance", async (req, res, next) => {
  try {
    const { bank_account_number } = req.body;
    let result = await db.any(
      `select balance from ${TABLE_ACCOUNT_BALANCE} where account_number = '${bank_account_number}'`
    );

    if (result.length == 0) {
      res.status(200).json({
        message: "Request Complete",
        data: { result: "No accounts found" }
      });
    } else {
      result = result[0];
      res.status(200).json({
        status: 200,
        message: "Balance Fetched",
        data: { result }
      });
    }
  } catch (error) {
    next(error);
  }
});

// Route to verify the UPI PIN of a bank account
// Body Parameters: { bankAccountNumber, upiID, upiPIN }
router.post("/verifyUPI", async (req, res, next) => {
  try {
    const { bankAccountNumber, upiID, upiPIN } = req.body;
    const { user_id } = req.user;

    let result = await db.any(
      `select upi_pin from ${TABLE_BANK_ACCOUNTS} where user_id = '${user_id}' and bank_account_number='${bankAccountNumber}' and upi_id='${upiID}'`
    );

    let compare_result = await bcrypt.compare(upiPIN, result[0].upi_pin);

    if (!compare_result) return res.status(404).json({ message: false });
    else {
      res.status(200).json({
        message: true
      });
    }
  } catch (error) {
    next(error);
  }
});

// UNUSED ROUTE
router.delete("/:id", async (req, res, next) => {
  const { id } = req.params;
  await db.none(`delete from ${TABLE_BANK_ACCOUNTS} where id = '${id}'`);

  res.status(200).json({
    status: 200,
    message: "Bank Account Deleted Successfully"
  });
});

module.exports = router;
